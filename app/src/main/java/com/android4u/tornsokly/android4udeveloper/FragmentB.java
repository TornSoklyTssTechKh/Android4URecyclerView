package com.android4u.tornsokly.android4udeveloper;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentB extends Fragment {

    TextView textView;
    ImageView imageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_b, container, false);
        textView = (TextView) v.findViewById(R.id.tv_show);
        imageView = (ImageView) v.findViewById(R.id.image);
        return v;
    }

    public void changeDescription(int position){
        String[] description = getResources().getStringArray(R.array.description);
        int[] images = {R.drawable.girl1,R.drawable.girl2,R.drawable.girl3,R.drawable.girl4,R.drawable.girl5,R.drawable.girl6,R.drawable.girl7,R.drawable.girl8,R.drawable.girl9,R.drawable.girl10};

        textView.setText(description[position]);
        imageView.setImageResource(images[position]);

    }


}
