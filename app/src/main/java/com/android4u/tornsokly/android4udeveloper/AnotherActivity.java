package com.android4u.tornsokly.android4udeveloper;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class AnotherActivity extends AppCompatActivity {

    FragmentB fragmentB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another);
        Intent intent= getIntent();
        int postion = intent.getIntExtra("POSITION",0);
        fragmentB =(FragmentB) getSupportFragmentManager().findFragmentById(R.id.fragment_b);
        fragmentB.changeDescription(postion);
    }
}
