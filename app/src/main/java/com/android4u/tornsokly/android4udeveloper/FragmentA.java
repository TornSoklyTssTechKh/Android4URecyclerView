package com.android4u.tornsokly.android4udeveloper;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentA extends Fragment implements AdapterView.OnItemClickListener{

    ListView listView;
    Communicator com;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_a, container, false);
        listView = (ListView) v.findViewById(R.id.listView);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(),R.array.chapter,android.R.layout.simple_list_item_1);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(FragmentA.this);
        return v;
    }

    public void setCommunicator(Communicator communicator){
        this.com = communicator;
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        com.respond(position);
    }


    public interface Communicator{
        public void respond(int position);
    }
}
