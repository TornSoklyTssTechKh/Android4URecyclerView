package com.android4u.tornsokly.android4udeveloper;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class MainActivity extends AppCompatActivity implements FragmentA.Communicator {

    FragmentA fragmentA;
    FragmentB fragmentB;
    FragmentManager manager;
    int pos=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manager = getSupportFragmentManager();
        fragmentA = (FragmentA) manager.findFragmentById(R.id.fragment);
        fragmentA.setCommunicator(this);
    }

    @Override
    public void respond(int position) {
        fragmentB = (FragmentB) manager.findFragmentById(R.id.fragment_b);
        if (fragmentB != null && fragmentB.isVisible()) {
            fragmentB.changeDescription(position);
            pos = position;
        } else {
            Intent intent = new Intent(this, AnotherActivity.class);
            intent.putExtra("POSITION", position);
            startActivity(intent);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("position", pos);
        Log.e("position",pos+"");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        int post;
        post = savedInstanceState.getInt("position");
        if( post !=0){
            Intent intent = new Intent(this, AnotherActivity.class);
            intent.putExtra("POSITION", post);
            startActivity(intent);
        }
    }
}
